---
title: "La commande cat sous Linux"
weight: 3
---

Cat dans Linux signifie concaténation (pour fusionner des choses ensemble) et est l'une des commandes Linux les plus utiles et les plus polyvalentes. Bien qu'elle ne soit pas exactement aussi mignonne et câline qu'un vrai chat, la commande Linux `cat` peut être utilisée pour prendre en charge un certain nombre d'opérations utilisant des chaînes de caractères, des fichiers et des sorties.

La commande cat a trois objectifs principaux impliquant des fichiers texte :

- Créer
- Lire/afficher
- Mise à jour/Modification

Nous allons passer en revue chacune d'entre elles pour montrer les commandes et les options associées à chaque opération.

### Pour commencer

Pour commencer, créons quelques fichiers appelés foo.txt et spam.txt.

Commençons par créer foo.txt avec la commande `cat > foo.txt` à partir de la ligne de commande Linux.

{{< button "/" "Warning" >}} S'il existe déjà un fichier nommé foo.txt, la commande `cat` utilisant l'opérateur > l'écrasera.

À partir de là, l'invite affichera une nouvelle ligne qui nous permettra de saisir le texte que nous voulons. Pour cet exemple, nous utiliserons :

```txt
FILE 1
foo
bar
baz
```

Pour revenir à la ligne de commande et créer le fichier texte, nous utilisons CTRL+d.

Maintenant, créons spam.txt suivnat cette seconde méthode :

```bash
cat <<EOF > spam.txt
FILE 2
spam
ham
eggs
EOF
```

Si nous voulions ajouter du texte à ces fichiers, nous utiliserions `cat >> FILENAME` et entrerions le texte que nous voulons utiliser.

Au lieu de devoir ouvrir un éditeur de texte, nous avons pu créer un fichier texte simple et rapide à partir de la ligne de commande, ce qui nous a permis d'économiser du temps et des efforts.

Le point essentiel de cette section est que nous utilisons `cat > FILENAME` pour créer ou écraser un fichier. De plus, nous pouvons utiliser `cat >> FILENAME` pour ajouter un fichier à un fichier déjà existant. Ensuite, après avoir tapé le texte que nous voulons, nous utilisons CTRL+d pour quitter l'éditeur, revenir à la ligne de commande et créer le fichier.

### Lire l'arc-en-ciel

Maintenant que nous avons créé quelque chose, regardons ce que nous avons fait.

Remarquez que nous n'avons pas d'opérateur > ou >> dans la commande suivante, seulement cat et le nom du fichier.

La commande `cat foo.txt` affichera ce qui suit :

```txt
FILE 1
foo
bar
baz
```

Donc, `cat foo.txt` nous permettra de lire le fichier, mais voyons ce que nous pouvons faire d'autre.

Disons que nous voulions savoir combien de lignes compte un dossier sur lequel nous travaillons. Pour cela, l'option -n est utile.

Avec la commande `cat -n foo.txt`, nous pouvons voir la longueur de notre fichier :

```txt
  1  FILE 1
  2  foo
  3  bar
  4  baz
```

Avec -n, nous pouvons avoir une idée du nombre de lignes du fichier sur lequel nous travaillons. Cela peut s'avérer utile lorsque nous itérons sur un fichier de longueur fixe.

### ConCaténer les dossiers

D'accord, nous avons vu que ce cat peut créer et afficher des fichiers, mais qu'en est-il de la concaténation (combinaison) ?

Pour cet exemple, nous utiliserons les fichiers foo.txt et spam.txt. Si nous voulons faire un peu de fantaisie, nous pouvons regarder le contenu des deux fichiers en même temps. Nous utiliserons à nouveau la commande cat, cette fois-ci en utilisant `cat foo.txt spam.txt` .

`cat foo.txt spam.txt` donne les résultats suivants :

```txt
FILE 1
foo
bar
baz
FILE 2
spam
ham
eggs
```

Notez que ce qui précède n'affiche que les deux fichiers. A ce stade, nous ne les avons pas encore concaténés en un nouveau fichier.

Pour concaténer les fichiers dans un nouveau fichier, nous voulons utiliser `cat foo.txt spam.txt > fooSpam.txt` qui nous donne le résultat dans un nouveau fichier fooSpam.txt

L'utilisation de `cat fooSpam.txt` produit les résultats suivants sur le terminal :

```txt
FILE 1
foo
bar
baz
FILE 2
spam
ham
eggs
```

Cette commande est également utile lorsque nous voulons concaténer plus de deux fichiers en un nouveau fichier.

Les points à retenir ici sont que nous pouvons visualiser plusieurs fichiers avec `cat FILENAME1 FILENAME 2`.

De plus, nous pouvons concaténer plusieurs fichiers en un seul avec la commande `cat FILENAME1 FILENAME 2 > FILENAME3`.

### Autres choses amusantes à faire avec cat(s)

Supposons que nous travaillons sur un fichier et que nous continuons à obtenir des erreurs pour une raison quelconque avant la fin du fichier - et il semble qu'il pourrait avoir plus de lignes que prévu.

Pour approfondir le fichier et éventuellement résoudre notre problème, nous pouvons utiliser le commutateur -A. L'option -A nous montrera où les lignes se terminent par un $, elle nous montrera les caractères de tabulation avec un ^I, et elle affiche également d'autres caractères non imprimables.

Si nous examinions un exemple de fichier texte non imprimable avec `cat nonPrintExample.txt`, nous pourrions obtenir quelque chose comme ceci :

```txt




       




```

Ce qui est bien, mais ne nous raconte peut-être pas toute l'histoire d'un personnage ou d'une chaîne qui pourrait nous causer des problèmes.

Alors que `cat -A nonPrintExample.txt` pourrait nous donner un résultat plus utile :

```txt
^I^I$
$
^L$
$
^G^H^H^H^Y^I^N^O^P^@$
^@^@^[g^[f^[d^[g^[6^[5^[4^[6^[=$
$
$
^X$
```

Ici, nous obtenons une représentation plus claire de ce qui peut se passer entre les tabulations, les retours à la ligne, les retours et les autres caractères.

Le point à retenir ici est que `cat -A FILENAME` peut nous donner des détails plus approfondis sur le dossier sur lequel nous travaillons.

