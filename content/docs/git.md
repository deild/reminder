---
title: "Git"
weight: 1
---

### Supprimer un sous-module

Les 3 étapes du processus de supression sont alors :

```bash
# optionnel, nécessaire pour l'étape 3bis
0. mv a/submodule a/submodule_tmp

1. git submodule deinit -f -- a/submodule
2. rm -rf .git/modules/a/submodule
3. git rm -f a/submodule
# Note: a/submodule (pas de slash)

# ou, si vous voulez le laisser dans votre arbre de travail et que vous avez fait l'étape 0
3. git rm --cached a/submodule
3bis mv a/submodule_tmp a/submodule
```

### Réécrire rapidement l'historique du dépôt git

utilisation de https://github.com/newren/git-filter-repo

Par exemple pour remplacer le nom `John` par `Jane`

```bash
git filter-repo --name-callback 'return name.replace(b "John", b "Jane")' --force
```
