---
title: "Vim"
weight: 2
---

### Comment commencer à écrire sur une ligne avec une indentation correcte

Avec cette petite astuce, vous n'aurez pas à faire de tabulation supplémentaire,
appuyez sur `S` il vous placera juste en mode insertion à la bonne indentation.

### Comment aller à la parenthèse/accolade correspondante

En mode NORMAL avec votre curseur sur une paranthèse carré [ ou ronde ( ou une accolade {,
appuyez sur `%` pour passer au symbole correspondant.
Appuyez à nouveau sur ce dernier pour revenir en arrière (passez d'un symbole à l'autre).

### Comment indenter/désindenter une ligne ou plusieurs lignes

```bash
>> - indenter une ligne
<< - désindenter une ligne
```

Lorsque vous avez sélectionné plusieurs lignes (en mode VISUAL),
il vous suffit d'appuyer une fois sur `>` ou `<` pour indenter ou désindenter les lignes.

### Comment corriger l'indentation dans l'ensemble du fichier

Commencez en haut d'un fichier (pour y arriver, appuyez sur `gg` n'importe où dans le fichier.)
Ensuite, tapez sur `=G`, et Vim fixera l'indentation dans tout le fichier.
Si vous ne commencez pas au début du fichier, il fixera l'indentation de la ligne courante jusqu'au bas du fichier.

### Les bases du travail avec les onglets

- `:tabnew` crée un nouvel onglet
- `:tabedit file` ouvre un nouvel onglet avec le fichier file
- `gt` passer à l'onglet suivant
- `gT` aller à l'onglet précédent
- `:tabonly` fermer tous les autres onglets excepté de celui qui est actif
- `:tabclose` fermer un onglet

### Rechercher et remplacer

`:s/black/white/` remplace la premiére occurrence de la chaine 'black' par 'white'

`:s/Ben\( Rogers\)\@!/Ben Rogers/g` remplace chaque occurrence de la chaine 'Ben' par 'Ben Rogers' sauf quand ' Rogers' est déjà présent

`:s/.*/<p>\r&\r<\/p>/` entoure la ligne entre `<p>` et `</p>`

`:-1s/–/\&mdash;/g` remplace chaque occurrence de la chaine '-' par '&mdash;' dans la ligne précédente

`:%s/SomeThing/something_else/gic` ici le flag i est utlisé pour la recherche case insensitive et le flag c pour la confirmation.

### Lorsque vous modifiez un fichier sans sudo

Si vous modifiez un fichier sans sudo, vous obtiendrez une erreur de permission en essayant de sauvegarder le fichier.
Vous devez donc l'enregistrer dans un autre fichier, puis effectuer les modifications à nouveau. Cependant, la commande :
`:w !sudo tee %`
sauvegardera le fichier sans avoir besoin de faire tout cela. Bien entendu, vous devez toujours être un utilisateur sudo pour utiliser cette commande.

### Exécuter toute commande

Exécutez n'importe quelle commande shell avec :
`:!<commande>`
